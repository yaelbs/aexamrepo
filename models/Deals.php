<?php

	namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;	
	
	class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
	{

	public $role; 
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

	public function rules()
    {
        return  
		[
            [['id', 'leadId','amount'], 'integer'],
			[['name', ], 'string', 'max' => 255],	
			[['id', 'id', ], 'required'],
			[['', 'updated_at', ], 'integer'],		
        ];				
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead Id',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }		
	
}
